package ru.t1consulting.vmironova.tm.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.vmironova.tm.enumerated.Sort;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public final class ProjectListRequest extends AbstractUserRequest {

    @Nullable
    private Sort sort;

    public ProjectListRequest(@Nullable String token) {
        super(token);
    }

}

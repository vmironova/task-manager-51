package ru.t1consulting.vmironova.tm.service.model;

import org.jetbrains.annotations.NotNull;
import ru.t1consulting.vmironova.tm.api.repository.model.ISessionRepository;
import ru.t1consulting.vmironova.tm.api.service.IConnectionService;
import ru.t1consulting.vmironova.tm.api.service.model.ISessionService;
import ru.t1consulting.vmironova.tm.model.Session;
import ru.t1consulting.vmironova.tm.repository.model.SessionRepository;

import javax.persistence.EntityManager;

public final class SessionService extends AbstractUserOwnedService<Session, ISessionRepository> implements ISessionService {

    public SessionService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    protected ISessionRepository getRepository(@NotNull final EntityManager entityManager) {
        return new SessionRepository(entityManager);
    }

}

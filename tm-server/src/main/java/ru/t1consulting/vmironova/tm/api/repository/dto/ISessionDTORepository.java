package ru.t1consulting.vmironova.tm.api.repository.dto;

import ru.t1consulting.vmironova.tm.dto.model.SessionDTO;

public interface ISessionDTORepository extends IUserOwnedDTORepository<SessionDTO> {
}

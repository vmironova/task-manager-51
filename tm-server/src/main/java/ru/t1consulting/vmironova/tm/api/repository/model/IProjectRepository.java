package ru.t1consulting.vmironova.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import ru.t1consulting.vmironova.tm.model.Project;

public interface IProjectRepository extends IUserOwnedRepository<Project> {

    @NotNull
    Project create(
            @NotNull String userId,
            @NotNull String name,
            @NotNull String description
    ) throws Exception;

    @NotNull
    Project create(
            @NotNull String userId,
            @NotNull String name
    ) throws Exception;

}
